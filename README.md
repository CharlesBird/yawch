## YAWCH - Yet Another Wrapper for Companies House API

### Motivation
I needed a simple bit of integration for the API provided by Companies House,
the UK Government regulator that tracks registration information for limited companies.

There are some wrappers around, but I wanted to handle multipage responses from 
the API which don't follow the JSONapi format. I was also trying to build API
queries into an app, but the response times were too slow for what I wanted, 
so it seemed like a good opportunity to explore a threads with callback model

### API specification ###

The API interface is specified at [https://developer.companieshouse.gov.uk/api/docs/](https://developer.companieshouse.gov.uk/api/docs/)

You will need to register for a free API key from the site

Note that the API is under active development, and there is no guarantee this
wrapper is synchronous with those changes

### Dependencies ###
This has been developed and tested under Python 3.5

It uses *requests*

### Testing ###

** Tests on returned values are vulnerable to changes in registered information **
** before running, check using the Companies House web interface that the **
** values in XXX are still correct **

Tests are written using *unittest* and executed either as stand-alone programs or via *nosetests*

### Warranty ###
**There is none.** This code may configure your computer to vote [Trump,Clinton] (delete as desired) in a mass botnet attack, transmogrify your cat into an alien or give you frankly erroneous results from the API. No responsibility is accepted for any/all of the above or frankly anything else. 

That said, issues relating to the third point would be welcomed. (I figure I'll hear about the first two anyway)