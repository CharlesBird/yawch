#! /usr/bin/env python3
#   
# yawch module tests
# (c) Charles Bird, 2016
#

# remember the test_ prefix is mandatory

try:
    import logging
except ImportError as e:
    print("Failed to load logging module: {:s}".format(e)) ## TODO push this to system level debugger
    raise

mod_logger=logging.getLogger(__name__)  ## wrong name here?
mod_logger.setLevel(logging.DEBUG)

try:
    import unittest
    import sys
    import os
    import datetime
    import requests
except ImportError as e:
    print("Failed to load system module: {:s}".format(e))
    raise

try:
    import yawch as yawch
except ImportError as e:
    print("Failed to load test module: {:s}".format(e))
    raise


class TestYawch(unittest.TestCase):    

    def setUp(self):
        self.api = yawch.CompaniesHouseAPI()
        ## need to add tests on api key problems
        
    def tearDown(self):
        pass
    
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass
    
    def test_company_number_format_succeeds(self):
        valid_registrations = [ "AB388651", "05388651" ]    # these are as of Oct 2016
        for reg in valid_registrations:
            with self.subTest( reg = reg):
                self.assertEqual( self.api.is_format_coy_num_valid(reg), True )
    
    def test_is_format_coy_num_valid_raises_error_on_wrong_type(self):
        non_string_registrations = [ 00000000, 88888888   ]    # these are as of Oct 2016
        for reg in non_string_registrations:
            with self.subTest( reg = reg):
                self.assertRaises( TypeError, self.api.is_format_coy_num_valid, coy_num_as_string = reg )
        
        
    def test_company_number_format_fails(self):
        invalid_registrations = [ 
            "5388651",      # truncated, fixable
            "ab388651",     # lower case, fixable
            "AB651",        # missing numbers in mixed reg format, fixable
            "A9388651",     # only one letter, right length overall
            "ABC88651",     # too many letters, right length overall
            "903886510",    # too long, numeric
            "003886510",    # too long, numeric, leading zero - currently not handled in regularisation
            "AB3886510",    # too long, mixed format
            "AB38865X",     # letters in wrong place
            "AB38865?",     # special characters
            "A>388657"      # special characters
            ]
        for reg in invalid_registrations:
            with self.subTest( reg = reg):
                self.assertEqual( self.api.is_format_coy_num_valid(reg), False )
        
    
    def test_working_company_number_regularisation(self):
        valid_conversions = {
            "AB388651":"AB388651",  # valid, check no change
            "05388651":"05388651",  # valid, check no change
            "5388651":"05388651",
            "ab388651":"AB388651",
            "ab651":"AB000651"
            }
        for cn in valid_conversions.keys():
            with self.subTest( cn = cn):
                self.assertEqual( self.api.regularise_coy_num(cn), valid_conversions[cn] )
        
    
    def test_company_number_regularisation_with_errors(self):
        failing_conversions = [ 
            "a9388651",
            "AB3886510",
            "AB38865X",
            "AB38865?",
            "A>388657",
            "003886510",    # may think about trimming excessive leading zeros in future
            "903886510",
            "AB0012345" 
            ]
        
        for cn in failing_conversions:
            with self.subTest( cn = cn ):
                self.assertRaises( ValueError, self.api.regularise_coy_num, coy_num_as_string = cn )
    
    def test_handle_error_on_no_api_key(self):
        pass
    
    def test_handle_error_on_wrong_api_key(self):
        pass
    
    def test_get_company_information(self):
        pass
        
    def test_get_officer_names(self):
        pass
    
    def test_match_officer_name(self):
        ## use a couple of example director sets here
        pass
        
    def test_no_match_officer_name(self):
        ## some close but not quite examples, as well as blanket fails
        pass
    
        
if __name__ == '__main__':
    progname= sys.argv[0].split(os.sep)[-1]
    print("Running {:s} as main test".format(progname))
    mod_logger.info("Running {:s} as main test".format(progname))
    
    unittest.main()
    
    
