#! /usr/bin/env python3
#
# authenticate control of new company for user
# (c) Charles Bird, 2016
#

try:
    import logging
except ImportError as e:
    print("Failed to load logging module: {:s}",format( str(e)) )     ## push this to system level debug log as well
    raise

mod_logger=logging.getLogger(__name__)  ## wrong name here?
mod_logger.setLevel(logging.DEBUG)      ## display etc depends on top level library handlers
    
try:
    import os
    import sys
    import re
    import requests
    import threading
    import pprint       ## development use only
except ImportError as e:
    mod_logger.error("Failed to load required system module: {:s}",format( str(e)) )
    raise

try:
    ## add package modules here
    import yawch_globals as yglobals         ## configuration values
except ImportError as e:
    mod_logger.error("Failed to load required module: {:s}",format( str(e)) )
    raise

    
class APIbase:

    _api_key = None
    _baseurl = None
    _verbose_debug = False
    _success_codes = set ( [ 200 ] )   ## http response codes, extend as needed
                                       ## should flag up 306s as represent change to API interface
                                       
    def process_api_key_uri(self, uri):
        ''' expects api-key as only line in file '''
        
        assert not hasattr(super(), 'process_api_key_uri')  # make sure base class is not masking non-derived method

        
        # split uri to get method and location
        
        ## we can either overload context manager for all uri types or do a bit of matching in an if/else tree
        
        ## ?? method for (KMS) encryption on AWS
        
        try:
            with open( uri ) as f:
                self._api_key = f.read().strip()
                if self._verbose_debug:
                    mod_logger.debug("APIbase.process_api_key_uri: file read of {:s} = {:s}".format( uri, self._api_key ))
            
            ## generic return from all uri methods
            return self._api_key
        except:
            raise
    
    def set_api_key(api_key):
        self._api_key = api_key;
    
    ## should move to the **kwds method to pass through any other variables
    def __init__(self, api_key = None, api_key_uri = None, verbose_debug=False):
        self._api_key = None
    
        if api_key:
            self._api_key = api_key
        elif api_key_uri:
            self._api_key = self.process_api_key_uri(api_key_uri)
        self._verbose_debug = bool(verbose_debug)
        
        
    
class CompaniesHouseAPI(APIbase):    ## lots more error handling needed
    ''' this API is way too unpredictable in response time to use in a synchronous
        manner, so we'll have to decouple this element of authorisation '''
        
    #_baseurl = "https://api.companieshouse.gov.uk"     # hard code fallback or let fail on misconfiguration?
        
    def __init__(self, *, api_key = None, api_key_uri = None, verbose_debug=False, base_url=None ):
        try:

            super().__init__(api_key, api_key_uri, verbose_debug)
                    
            if 'baseurl' in yglobals.coy_hse_api.keys(): ## or let misconfiguration fail??
                self._baseurl = yglobals.coy_hse_api['baseurl']
            if base_url:
                self._baseurl = base_url
        except KeyError as e:
            mod_logger.error("CoyHse api configuration dict missing required key: {:s}".format(api_method))
            raise

## how should we design an async service here, so that callbacks and data can exist in common space
## and can persist elements of dataset between calls to fetch page

    
    
    def query_api_as_json(self, api_str):       ## how do we handle callback for this function
    
    ### api_str is the base query - it will be modified within this function to handle multi-page returns
    ### by appending '&start_index=XXX&items_per_page=NNN' as required
    
    ## could generalise further by passing the correct authentication object here or in base
    
        return_result = True        ### dummy var at moment to return collected json from api
        
        try:
            url = "".join( (self._baseurl, api_str) )
    
    ### will need to put a while loop here to track the total number of items collected
    ### will also need to work out whether to separate the page meta data from the data items
    ### themselves
    
            r = requests.get( url, auth=( self._api_key, ''))    

            coy_json = r.json()
                
            if self._verbose_debug:
                print("------------------------------------------")
                print("http call: {:s}".format(url))
                print("status code: {:d}".format(r.status))
                ## pprint.pprint( coy_json )
                print("------------------------------------------") 
                
## could this be multiple page??                

## this multi-page behaviour is not generic beyond companies house                
## need to think about to handle partial sets (eg to cope with limit on 200 increments of start index and rate limits)                
## and retry behaviour on specific errors here
                
            if r.status_code in self._success_codes:   ## ?? other codes 
                return_result = True
                
                print ("query_api_as_json: ", coy_json.keys() )    
##
## should we map companies house onto json 1.0 ontology??
## ['total_results', 'kind', 'page_number', 'items_per_page', 'start_index', 'items']
                
                print ("\n\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+")
                


                ##for k in ['items_per_page', 'total_results', 'start_index' ]:
            
## end of the loop somewhere here
            
            if return_result:   ## fake variable for the moment
                return ( coy_json )
            else:
                return None             ## what's right thing to do here
        
        except requests.exceptions.ConnectionError as e:
            print("Connection attempt to {:s} failed".format(url))
            mod_logger.error("Connection attempt to {:s} failed".format(url))
        except requests.exceptions.Timeout as e:
            print("Connection to {:s} timeout".format(url))
            mod_logger.error("Connection attempt to {:s} failed".format(url))
            
        except:
            raise
    
## this is actually hooking into generic search - it needs to handle pagination issues in constructing return elements
## and position these as wrappers
            
    def fetch_info_for_company_number(self, coy_num_as_string): ### company 'number' is actually a string with leading 0 and possibly alpha char
        try:
            api_method = 'searchCoyNum'
            
            ## add checks here
            
            api_call = yglobals.coy_hse_api[ api_method ].format(coy_num_as_string)     ## use the AttributeError on missing key
            
            json = self.query_api_as_json(api_call)
            if self._verbose_debug:
                mod_logger.debug("query_company_number keys: {:s}", str(json.keys()) )
            return json
            
        except KeyError as e:
            mod_logger.error("CoyHse api configuration dict missing required key {:s}".format(api_method))
            raise
        
    def build_next_page_element(self, total_items, items_per_page, current_page):
        pass
    
    
# using this to develop the general multipage handler - will also need to think about how to handle threading better    
    def query_generic_company_info (self, query_str):
        try:

            api_method = 'searchGeneric'
            ### does this do the required html escaping ????? TODO
            api_call = yglobals.coy_hse_api[ api_method ].format( str(query_str) )
            url = "".join ((self._baseurl, api_call ))   #####
    

## this didn't raise on failed connect    
            r = requests.get( url, auth=( self._api_key, ''))    

            coy_json = r.json()
                
            if self._verbose_debug:
                print("http call: {:s}".format(url))
                print("status code: {:d}".format(r.status))
                pprint.pprint( coy_json )

## could this be multiple page??                
                
            if r.status_code in self._success_codes:   ## ?? other codes 
            
                print ("IN GENERIC SEARCH")
                print (url)
                print ( coy_json.keys() )
                for k in ['items_per_page', 'total_results', 'start_index', 'page_number', 'kind' ]:
                    print (k,": ",coy_json[k])
            
                print( self.build_next_page_element () )
            
                return ( coy_json )
            else:
                return None             ## what's right thing to do here
        
        except KeyError as e:
            mod_logger.error("CoyHse api configuration dict missing required key: {:s}".format(api_method))
            raise
        
        except requests.exceptions.ConnectionError as e:
            print("Connection attempt to {:s} failed".format(url))
            mod_logger.error("Connection attempt to {:s} failed".format(url))
            
        except:
            raise
                
    def is_format_coy_num_valid(self, coy_num_as_string):
        # see pad_company_number for explanation of expected format (Oct 2016)
        # yes, this could be compressed, but given the format is expected to change
        # chosen to leave elements accessible to aid future development
        
        if type('string') != type(coy_num_as_string):   ## naive conversion here could lose leading zeros
            raise TypeError("is_format_coy_num_valid should be passed string representation of company registration")
            
        is_coy_num_in_spec = re.match(yglobals.coy_hse_num_spec['CoyNumFmt'], coy_num_as_string)
        
        if self._verbose_debug:
            mod_logger.debug("is_coy_num_valid_format, coy_num: {:s}".format(coy_num_as_string) )
            mod_logger.debug("is_coy_num_valid_format, format: {:s}".format(yglobals.coy_hse_num_spec['CoyNumFmt']) )
            mod_logger.debug("is_coy_num_valid_format, is_coy_num_spec: {:s}".format(str(is_coy_num_in_spec) ))
        
        if is_coy_num_in_spec:
            return True
        else:
            return False
        
                
    def regularise_coy_num(self, coy_num_as_string):            
    # attempts to fix a company number following the developer forum advice below            
    # http://forum.aws.chdev.org/t/api-returns-no-match-for-48839-but-does-for-00048839-is-there-a-specification-for-company-numbers/624
    # to match the format regexp in the defaults file
    #
    # remember, this is a best guess at what has gone wrong with number - there is no checksumming etc
    # and the Companies House expectation is that the registration will be derived from search
    #
    
        try:
            # make sure we have a string, with any letters in uppercase
            if type('string') != type(coy_num_as_string):
                coy_num = str(coy_num_as_string).upper()
            else:
                coy_num = coy_num_as_string.upper()
                
            coy_num_len = len(coy_num)
            
            if yglobals.coy_hse_num_spec['CoyNumLen'] < coy_num_len:     ##  likely a data entry foul up
                                                                        ##  could think about truncating any leading zeros idc
                raise ValueError("Cannot regularise overlength company number {:s}".format(coy_num))
            
            all_numeric = r"^[0-9]+$"
            is_all_numeric = re.match( all_numeric, coy_num)
            leading_two_ltrs = r"^([A-Z]{2})([0-9]+)$"
            has_lead_two_ltrs = re.match( leading_two_ltrs, coy_num)
                        
            if yglobals.coy_hse_num_spec['CoyNumLen'] == coy_num_len and is_all_numeric:
                pass
            elif yglobals.coy_hse_num_spec['CoyNumLen'] == coy_num_len and has_lead_two_ltrs: 
                pass
            elif yglobals.coy_hse_num_spec['CoyNumLen'] > coy_num_len and is_all_numeric:
                coy_num = coy_num.zfill( yglobals.coy_hse_num_spec['CoyNumLen'] )
            elif yglobals.coy_hse_num_spec['CoyNumLen'] > coy_num_len and has_lead_two_ltrs:
                if self._verbose_debug:
                    mod_logger.debug("Padding partial number in coy_num")
                    mod_logger.debug("Alpha = {:s}, numbers = {:s}".format(has_lead_two_ltrs.group(1),str(has_lead_two_ltrs.group(2))) )
                # pad only numeric element of too short mixed format registration
                num_ele = has_lead_two_ltrs.group(2).zfill( yglobals.coy_hse_num_spec['CoyNumLen'] - len(has_lead_two_ltrs.group(1)) )
                coy_num = "".join( ( has_lead_two_ltrs.group(1), num_ele )  )
            else:
                ### too mangled to make a sensible correction
                raise ValueError("Cannot regularise company number {:s}".format(coy_num))
                
            return coy_num
        except KeyError as e:
            mod_logger.error("Coy Hse api missing required config value {:s}".format(str(e)))
            raise
                
                
    def query_company_officers(self, coy_num):
        try:
            api_method = 'CoyOfficers'
            api_call = yglobals.coy_hse_api[ api_method ].format(str(coy_num))
            if self._verbose_debug:
                print("In query_company_officers: ".format(api_call) )
            url = "".join ((self._baseurl, api_call ))
           
            r = requests.get( url, auth=( self._api_key, ''))    
            #   print (r.status_code, ": ", type(r.status_code ))
       
            coy_json = r.json()         ## make a copy to save on function call
                                        ## could use a local cache on these as well
            if self._verbose_debug:
                mod_logger.debug("http call: {:s}".format(url))
                mod_logger.debug("status code: {:d}".format(r.status))
                pprint.pprint( coy_json )   ## this doesn't play well with logging

## need to extend to handle multiple page returns ******             
                
            if r.status_code in self._success_codes:   ## ?? other codes 
                return ( coy_json )
            else:
                return None             ## what's right thing to do here
        except KeyError as e:
            mod_logger.error("CoyHse api configuration missing required method: {:s}".format(api_method) )
            raise
        except:
            raise

    def isCompanyActive(self, coy_num):
        pass
            
    def get_active_officer_names(self, coy_num):
        try: 
            officers = self.query_company_officers(coy_num)
            
            if officers:
                names = list()
            
                officers_keys = officers.keys()
                
                
                print("in get_active_officer_names")
                print (officers.keys() )
                print("Total results: {:d}".format(officers['total_results']))
                
                ### items is the array of information
                #if 'items' in officers_keys:
                #    print("Items: {:s}".format( str(officers['items']) ) )
                #else:
                #    print("No items value returned")
                
                print("Items per page: {:d}".format(officers['items_per_page']))
                print("Start index: {:d}".format(officers['start_index']))
                
                for i in officers['items']:
                    
                    if "resigned_on" not in i.keys():
                        names.append (i['name'])
                
                if len(names) != officers['active_count']:
                    ## value error or runtime ?
                    raise ValueError("Parsing error - num active officers {:d} <> expected value {:d}".format(len(names), officers['active_count'] ))
            
                if len(names) > 0:
                    return names
                else:
                    mod_logger("No active officers found for coy_num {:d}".format(coy_num))
                    return None
                
            else:
                mod_logger("No officer records found for coy_num {:d}".format(coy_num))
                return None
        except AttributeError as e:
            mod_logger.error("Unable to parse malformed officer data for company {:d}: {:s}".format(coy_num, e) )
            raise
            
    def is_user_active_officer_in_coy(self, user_dict, coy_num):    ## is this boolean or should we return a score/enum
                                                                    ## eg on former vs current names
        
        officer_names = self.get_active_officer_names(coy_num)
        
        if officer_names:
        
            ## break out the ident matching functionality as utility function
                    
            for o in officer_names:
                if self.names_match(user_dict, o):
                    return True
            return False    
        else:
            return False    

    def names_match(self, user_dict, coy_hse_name_str):     ## isolate company house representation
                                                            ## which is likely to change
                                                            ## can also add in former name checks
                                                            ## this is NOT tested against unicode/non-latin
        try:
            (coy_hse_family, coy_hse_given) = coy_hse_name_str.split(",")
            if self._verbose_debug:
                mod_logger.debug("names_match: coy_hse data -> {:s}:{:s}".format(coy_hse_family, coy_hse_given))
                mod_logger.debug("names_match: user_dict -> {:s}:{:s}".format(user_dict['FamilyName'],user_dict['GivenName']) )
            
            if user_dict['FamilyName'].upper() != coy_hse_family:
                return False    # no match on family name
            
            # so here the family name has matched, now check permutation on given names
                        
            for given in user_dict['GivenName'].upper().lstrip().split(" "):
                for name in coy_hse_given.upper().lstrip().split(" "):
            
                    if self._verbose_debug:
                        mod_logger.debug("Loop: {:s} ~ {:s}".format( given, name ) )
                    if given == name:
                        return True     ## family and at least one given match
            
            return False    ## the family name matched but none of the given
        except: # catch attribute error on user dict
            raise
        
            
def main():
    ## obviously this would be passed in somehow
    ## and in lambda context would be in s3 bucket with restrictive permissions etc
    api_file = r"C:\Users\Charlie\Documents\api_keys\coy_house_api_key.txt"
    wynd_gdn_cntr_num = "05388651"    ## how does leading zero behave
                                      ## company numbers can also be alphanumeric
    trunc_num = "5388651"
    alpha_num = "AB388651"
    case_num = "ab388651"
    ok_ltr_missing_num = "ab651"
    only_one_ltr = "a9388651"
    too_long_num = "AB3886510"
    ectopic_ltrs = "AB38865X"
    spec_char1 = "AB38865?"
    spec_char2 = "A>388657"
    wynd_gdn_cntr_pcode = "NR12 9PD"
    wynd_dir_user={'FamilyName':'Groucott', 'GivenName':'Gary Sydney'}
    ## need to be careful with localisation
 
    print ("Demo code")
    ################### 
    CoyHse = CompaniesHouseAPI(api_key_uri = api_file, verbose_debug=True)
  
    company_number = "05388651"     # random actual company
        
    if CoyHse.is_format_coy_num_valid( company_number ):
        print (company_number, " is in a valid format")
    
    ###################
        
    if False:
        print("Using wyndmondham garden centre test example")
        print("=====")
        print (CoyHse.query_company_number(wynd_gdn_cntr_num) )
    if False:
        print("++++")
        print (CoyHse.query_company_officers(wynd_gdn_cntr_num) )
        print("~~~~~")
        print (CoyHse.get_active_officer_names(wynd_gdn_cntr_num) )
        print("|||||||||||")
        print(wynd_dir_user)
        print (CoyHse.is_user_active_officer_in_coy(wynd_dir_user, wynd_gdn_cntr_num) )
        print("next director")
        print ("Brenda")
        print (CoyHse.is_user_active_officer_in_coy({'FamilyName':'gRoucott','GivenName':'margaret'}, wynd_gdn_cntr_num) )
        print("=================")
        print("=================")
    #print (CoyHse.query_generic_company_info("London") )
#    CoyHse.query_generic_company_info("London") 
    #print("=====")
    #print (CoyHse.query_company_number(wynd_gdn_cntr_pcode) )
    
    ## for production lambda will need to push this down an async route - either dynamo with stream or SQS/SWF message
    
    
if "__main__" == __name__:
    progname= sys.argv[0].split(os.sep)[-1]
    mod_logger.info("Running {:s} as main".format(progname) )
    main()