#! /usr/bin/env python3

#
# config options - will have override options on cmd line idc
# (c) Charles Bird, 2016
#
#

## this holds the api queries - ultimately this should be a dynamic return from site
## or use something like swagger or yaml


# the spec for the company "number" are from Oct 2016
# see following entry from Companies House developers forum
# http://forum.aws.chdev.org/t/api-returns-no-match-for-48839-but-does-for-00048839-is-there-a-specification-for-company-numbers/624
#
coy_hse_num_spec = {
    'CoyNumLen':8,
    'CoyNumFmt':r"^([A-Z]{2}|[0-9]{2})[0-9]{6}$"    ## need conditional check on 1st two characters
    }

## use the .format syntax for placeholders
## note that pagination is handled within the api, so only enter the base API resource string here
coy_hse_api = {
    'baseurl':"https://api.companieshouse.gov.uk",  # fully qualified URL (protocol, port if non-standard and host)
    'search':"/search/companies/?q=",
    'searchGeneric':"/search/companies/?q={:s}",    # any search string
    'searchCoyNum':"/search/companies/?q={:s}",     # company number as string - remember can have leading pair of letters or leading zeros
    'CoyOfficers':"/company/{:s}/officers"          # company number as string
    }
